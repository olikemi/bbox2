#!/bin/python2.7
'''
Please Read the readme file before using this script!!!

bboxconnect.py
Copyright (C) 2013 Olikemi [ at ] gmail . com
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

'''

import mechanize
import md5
import sys

##Mechanize work with Python 2.7
url = 'http://192.168.1.1' #Change if your bbox is not at this address
username  = 'user'  # default BBOX user is user
password  = '' #Enter your password between '' in CAPITAL LETTER (serial number of the bbox)

if not password:
	print ("You must enter the BBOX Password on the script password='xxxxx'")
	exit(1)

def syntax():
    print 'Syntax: ./bboxconnect.py [ON/OFF]'
    print
    print 'Use "on" argument the Power ON the modem wifi'
    print 'Use "off" argument the Power OFF the modem wifi'
    print


for arg in sys.argv:
	setwifi=arg
	print (setwifi.lower())

if setwifi.lower() in "on":
	print ("Log and set on wireless")
elif setwifi.lower() in "off":
	print ("Log and set off wireless")
else:
	syntax()
	exit(1)

#Start Browser
br = mechanize.Browser()
response = br.open(url)

'''
# List the forms that are in the page
for form in br.forms():
    print "Form name:", form.name
    print form
'''

# Selecte a form (only 1 in welcome page)

br.form = list(br.forms())[0]  # use when form is unnamed
br.set_all_readonly(False)    # allow everything to be written to
br.set_handle_robots(False)   # ignore robots
br.set_handle_refresh(False)  # can sometimes hang without this
br.addheaders = [('User-agent', 'Firefox')]

for control in br.form.controls:
	#print control
	#print "type=%s, name=%s value=%s" % (control.type, control.name, br[control.name])
	if "session_id" in control.name:
		sessionid=br[control.name]
	if control.name == "auth_key":
		auth=br[control.name]


##Adaptation from welcomepage javascript
passwordsession="password_"+sessionid
br.form[passwordsession] = ""
passauth=password+auth
m = md5.new()
m.update(passauth)
passwordauthmd5=m.hexdigest()
br.form["md5_pass"] = passwordauthmd5

response = br.submit() #Send the password and log in the modem

#print response.read() #Debug use

##Go to the wireless page
br.form = list(br.forms())[0]
br.find_control('mimic_button_field').readonly = False
br['mimic_button_field'] = 'sidebar: lb_sidebar_advanced_wireless..'
response = br.submit()
#print response.read() #Debug use

##Set wifi on/off
br.form = list(br.forms())[0]
br.find_control('mimic_button_field').readonly = False
if setwifi.lower() in "on":
	br['mimic_button_field'] = 'submit_button_activate: ..'
	print ("Wireless ON !")
elif setwifi.lower() in "off":
	br['mimic_button_field'] = 'submit_button_deactivate: ..'
	print ("Wireless OFF !")
response = br.submit()



br.back()   # End

