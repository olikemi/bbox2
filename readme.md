This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.


###Set the wireless ON/OFF with Telnet###
Wifion.sh and wifioff.sh work only in Linux with netcat (nc) program.
This program connect to the bbox2  in telnet and send the command.

###Set the wireless ON/OFF with the bbox webpage###
bboxconnect.py is written in python. I test it in Linux but should work in Windows.
You have to install mechanize before using the script 
URL http://wwwsearch.sourceforge.net/mechanize/

These 3 variable at the begining of the script should be adapated to your config:
url = 'http://192.168.1.1' #Change if your bbox is not at this address

username  = 'user'  # default BBOX user is user

password  = '' #Enter your password between '' in CAPITAL LETTER (serial number of the bbox)


