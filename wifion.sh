#!/bin/bash
host=192.168.1.1
port=23
login=admin
#Replace MYSECRETPASSWORD by the bbox2 password (serial number) in capital letter
pass=MYSECRETPASSWORD

commands()
{
        echo "$login";
        sleep 1
        echo "$pass";
        sleep 1
        echo "rg_conf_ram_set dev/eth2/volatile_enabled 1";
        sleep 1
        echo "rg_conf_set dev/eth2/enabled 1";
        sleep 1
        echo "reconf 1";
        sleep 1
        echo -e "exit";
}

commands | nc $host $port

exit 0 
